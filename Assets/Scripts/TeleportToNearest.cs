using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TeleportToNearest : Teleport
{
	public Transform[] arrivalTargets;

	protected override void OnTriggerEnter(Collider other)
	{

		var targets = arrivalTargets
			.Where(t => t.gameObject.activeSelf)
			.OrderBy(t => (t.position - other.gameObject.transform.position).magnitude);

		if(targets.Count() == 0)
			return;

		DoTeleport(other.gameObject, targets.First());
	}
}