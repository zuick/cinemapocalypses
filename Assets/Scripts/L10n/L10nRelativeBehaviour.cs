using UnityEngine;
using CinemaPocalypses.Events;
using GameEventBus;
using CinemaPocalypses.L10n;

public class L10nRelativeBehaviour : MonoBehaviour
{
	protected virtual void Awake()
	{
		EventBus.Instance.Subscribe<LocaleChanged>(OnLocaleChanged);
		if (L10nManager.Loaded)
			L10nReady();
		else
			L10nManager.OnLoaded += L10nReady;
	}

	protected virtual void L10nReady() { }

	protected virtual void OnLocaleChanged(LocaleChanged e) { }

	protected virtual void OnDestroy()
	{
		EventBus.Instance.Unsubscribe<LocaleChanged>(OnLocaleChanged);
		L10nManager.OnLoaded -= L10nReady;
	}
}