using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Globalization;
using CinemaPocalypses.Events;
using GameEventBus;
using System;

namespace CinemaPocalypses.L10n
{
	public class L10nManager : Singleton<L10nManager>
	{
		public TextAsset l10n;
		public int startRow = 1;
		public static Action OnLoaded;
		public static bool Loaded { private set; get; }
		public static string[] Locales { private set; get; }
		public static string CurrentLocale
		{
			set
			{
				if (CurrentLocale == value)
					return;

				if (!Locales.Contains(value))
					return;

				PlayerPrefs.SetString(playerPrefsLocale, value);
				PlayerPrefs.Save();

				EventBus.Instance.Publish(new LocaleChanged { locale = value });
			}
			get
			{
				return PlayerPrefs.GetString(playerPrefsLocale, "");
			}
		}

		private const string LINE_SEPARATOR = @"\n\n";
		private static string playerPrefsLocale = "locale";
		private static string defaultLocale = "en";
		private static string buttonReplacePattern = "<button>([0-9a-zA-Z ]*)</button>";
		private static string buttonReplacement = "<color='#{1}'>{0}</color>";
		private static Regex buttonReplaceRegex = new Regex(buttonReplacePattern, RegexOptions.IgnoreCase);

		private static Dictionary<string, Dictionary<string, string>> data = new Dictionary<string, Dictionary<string, string>>();
		private static Dictionary<string, string> localeNames = new Dictionary<string, string>();

		protected override void Awake()
		{
			base.Awake();

			LoadData();
			LoadCultureInfo();

			var initialLocale = defaultLocale.Length == 0
				? GetLocale(Application.systemLanguage.ToString())
				: defaultLocale;

			if (CurrentLocale == "" &&
				Locales.Length > 0 &&
				Locales.Contains(initialLocale)
			)
				CurrentLocale = initialLocale;

			Loaded = true;
			OnLoaded?.Invoke();
		}

		public static string Get(string key, bool allowEmpty = false)
		{
			var locale = CurrentLocale;
			var keyLowCase = key.ToLower().Trim();
			if (!data.ContainsKey(locale))
				return key;

			if (data[locale].ContainsKey(keyLowCase))
			{
				var value = data[locale][keyLowCase];
				if (string.IsNullOrWhiteSpace(value))
					return allowEmpty ? "" : key;
				else
					return data[locale][keyLowCase];
			}

			return key;
		}

		public static string Get(string key, params string[] args)
		{
			var text = Get(key);
			try
			{
				text = string.Format(text, args);
			}
			catch (Exception e)
			{
				Debug.LogError($"Can't format key {key}, args not correct \n {e.ToString()}");
				text = key;
			}
			return text;
		}

		public static string GetLocaleName(string locale)
		{
			if (localeNames.ContainsKey(locale))
				return localeNames[locale];

			return locale;
		}

		public static string GetLocale(string name)
		{
			foreach (var pair in localeNames)
			{
				if (pair.Value == name)
					return pair.Key;
			}
			return defaultLocale;
		}

		public static string[] SplitByLines(string str)
		{
			return str.Split(
				new string[] { LINE_SEPARATOR }, System.StringSplitOptions.RemoveEmptyEntries
			);
		}

		public static string RandomLine(string str)
		{
			var lines = SplitByLines(str);
			if (lines.Length == 0)
				return "";

			return lines[UnityEngine.Random.Range(0, lines.Length)];
		}

		private void LoadData()
		{
			if (l10n == null)
			{
				Debug.LogError("There is no l10n file");
				return;
			}

			var rows = l10n.text.Split('\n');
			if (rows.Length <= startRow)
				return;

			Locales = rows[0]
				.Split('\t')
				.Skip(1)
				.Select(c => c.Trim())
				.ToArray();

			foreach (var locale in Locales)
				data.Add(locale, new Dictionary<string, string>());

			foreach (var row in rows.Skip(startRow))
			{
				var cols = row.Split('\t');
				if (cols.Length <= 1)
					continue;

				var key = cols[0].ToLower().Trim();
				for (var i = 1; i < cols.Length; i++)
				{
					if (i < Locales.Length + 1)
						data[Locales[i - 1]].Add(key, cols[i]);
				}
			}
		}

		private static void LoadCultureInfo()
		{
			var cinfos = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);
			foreach (var c in cinfos)
			{
				var code = c.Name.Split('-')[0];
				if (!localeNames.ContainsKey(code))
					localeNames.Add(code, c.DisplayName);
			}
		}
	}
}