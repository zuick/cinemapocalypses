using UnityEngine;
using UnityEngine.UI;
using CinemaPocalypses.L10n;
using CinemaPocalypses.Events;

public class L10nText : L10nRelativeBehaviour
{
	public string key = "";

	protected override void L10nReady()
	{
		UpdateText();
	}

	protected override void OnLocaleChanged(LocaleChanged e)
	{
		UpdateText();
	}

	private void UpdateText()
	{
		var textComponent = GetComponent<Text>();
		if (textComponent != null)
			textComponent.text = L10nManager.Get(key).ReplaceNewLine();
	}
}