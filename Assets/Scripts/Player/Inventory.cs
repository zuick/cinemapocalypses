using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using CinemaPocalypses.Events;
using GameEventBus;
using UnityEngine.Events;

namespace CinemaPocalypses
{
	public class Inventory : MonoBehaviour
	{
		public Transform head;
		public Transform handPivot;
		public Vector3 inventoryWorldPosition;

		public float throwForce = 400f;
		public Vector3 throwDirection;
		public MouseButton throwMouseButton;

		public int capacity = 9;

		public float mouseWheelMinDelta = 0.1f;
		public float mouseWheelCheckInterval = 0.05f;

		public UnityEvent OnAdd = new UnityEvent();
		public UnityEvent OnRemove = new UnityEvent();

		private List<Grabable> items = new List<Grabable>();
		private int selectedItemIndex = -1;
		private bool awaitMouseWheelInput = false;

		private void Awake()
		{
			for (var i = 0; i < capacity; i++)
				items.Add(null);

			Select(0);
		}

		private void Start()
		{
			PublishUpdate();
		}

		public void Add(Grabable grabable)
		{
			if (items.Contains(grabable))
				return;

			var firstEmptyCell = items.FindIndex(item => item == null);

			if (firstEmptyCell < 0)
				return;

			grabable.body.isKinematic = true;
			grabable.body.detectCollisions = false;
			grabable.body.velocity = Vector3.zero;
			grabable.transform.SetParent(null);
			grabable.transform.position = inventoryWorldPosition;

			items[firstEmptyCell] = grabable;


			if (selectedItemIndex == firstEmptyCell)
				Select(selectedItemIndex);
			else
				PublishUpdate();

			OnAdd.Invoke();
		}

		public void Unselect(int index)
		{
			if (index < 0 || index >= items.Count)
				return;

			selectedItemIndex = -1;

			var grabable = items[index];
			if (grabable == null)
				return;

			grabable.transform.SetParent(null);
			grabable.transform.position = inventoryWorldPosition;
		}

		public void Select(int index)
		{
			if (index < 0 || index >= items.Count)
				return;

			if (selectedItemIndex >= 0)
				Unselect(selectedItemIndex);

			selectedItemIndex = index;

			PublishUpdate();

			var grabable = items[index];
			if (grabable == null)
				return;

			grabable.transform.SetParent(handPivot);
			grabable.transform.localPosition = Vector3.zero;
			grabable.transform.localScale = Vector3.one;
			grabable.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
		}

		public void Remove(int index)
		{
			if (index < 0 || index >= items.Count)
				return;

			var grabable = items[index];
			if (grabable == null)
				return;

			grabable.transform.SetParent(null);
			grabable.body.isKinematic = false;
			grabable.body.detectCollisions = true;
			grabable.transform.localScale = Vector3.one;
			grabable.body.AddForce((head.forward + throwDirection.normalized) * throwForce, ForceMode.Force);
			items[items.IndexOf(grabable)] = null;

			PublishUpdate();

			OnRemove.Invoke();
		}

		private void Update()
		{
			if (Input.GetMouseButtonDown((int)throwMouseButton))
				Remove(selectedItemIndex);

			if (!awaitMouseWheelInput && Mathf.Abs(Input.mouseScrollDelta.y) > mouseWheelMinDelta)
			{
				Select(NextIndex(Input.mouseScrollDelta.y > mouseWheelMinDelta ? -1 : 1));
				StartCoroutine(AwaitMouseWheelInputCoroutine());
			}

			if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
				Select(0);
			if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
				Select(1);
			if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
				Select(2);
			if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
				Select(3);
			if (Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5))
				Select(4);
			if (Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Keypad6))
				Select(5);
			if (Input.GetKeyDown(KeyCode.Alpha7) || Input.GetKeyDown(KeyCode.Keypad7))
				Select(6);
			if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Keypad8))
				Select(7);
			if (Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9))
				Select(8);
			if (Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0))
				Select(9);
		}

		private IEnumerator AwaitMouseWheelInputCoroutine()
		{
			yield return new WaitForSeconds(mouseWheelCheckInterval);
			awaitMouseWheelInput = false;
		}

		private int NextIndex(int delta)
		{
			return Mathf.Max(0, Mathf.Min(items.Count - 1, selectedItemIndex + delta));
		}

		private void PublishUpdate()
		{
			EventBus.Instance.Publish(new InventoryChanged
			{
				items = items.Select(i => i).ToList(),
				selectedItemIndex = selectedItemIndex
			});
		}
	}
}