﻿// FPS Controller
// 1. Create a Parent Object like a 3D model
// 2. Make the Camera the user is going to use as a child and move it to the height you wish. 
// 3. Attach a Rigidbody to the parent
// 4. Drag the Camera into the m_Camera public variable slot in the inspector
// Escape Key: Escapes the mouse lock
// Mouse click after pressing escape will lock the mouse again


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FPSControllerStates { Staying, Walking, Running, InJumping }

[RequireComponent(typeof(Rigidbody))]
public class FPSController : MonoBehaviour
{
	public Camera playerCamera;
	public float speed = 5.0f;
	public float acceleration = 1.5f;
	public float inAirMovementFactor = 0.3f;
	public float jumpForce = 100f;
	public LayerMask groundLayers;
	public Transform groundCheckPosition;
	public float groundCheckRadius = 0.1f;
	public float smoothSpeed = 10f;
	public float lookSensitivity = 3.0f;

	public FPSControllerStates State
	{
		get
		{
			if (!IsGrounded)
				return FPSControllerStates.InJumping;
			if (m_velocity.magnitude < 0.5f)
				return FPSControllerStates.Staying;
			if (m_velocity.magnitude <= speed)
				return FPSControllerStates.Walking;

			return FPSControllerStates.Running;
		}
	}

	public bool IsGrounded { private set; get; }


	public float Speed => m_velocity.magnitude;
	private float m_MovX;
	private float m_MovY;
	private Vector3 m_moveHorizontal;
	private Vector3 m_movVertical;
	private Vector3 m_velocity;
	private Rigidbody m_Rigid;
	private float m_yRot;
	private float m_xRot;
	private Vector3 m_rotation;
	private Vector3 m_cameraRotation;
	private float m_cameraInitialRotation;
	private float actualSpeed;

	// Use this for initialization
	private void Start()
	{
		m_Rigid = GetComponent<Rigidbody>();
		m_cameraInitialRotation = playerCamera.transform.rotation.eulerAngles.x;
	}

	// Update is called once per frame
	public void FixedUpdate()
	{
		if (Time.timeScale <= 0) return;

		var acc = IsGrounded
			? Input.GetButton("Run") ? acceleration : 1f
			: inAirMovementFactor;

		m_MovX = Input.GetAxis("Horizontal");
		m_MovY = Input.GetAxis("Vertical");

		m_moveHorizontal = transform.right * m_MovX;
		m_movVertical = transform.forward * m_MovY;

		m_velocity = Vector3.Lerp(m_velocity, (m_moveHorizontal + m_movVertical).normalized * speed * acc, smoothSpeed * Time.deltaTime);

		m_yRot = Input.GetAxisRaw("Mouse X");
		m_rotation = new Vector3(0, m_yRot, 0) * lookSensitivity;

		m_xRot = Input.GetAxisRaw("Mouse Y");
		m_cameraRotation = new Vector3(m_xRot, 0, 0) * lookSensitivity;

		if (m_velocity != Vector3.zero)
			m_Rigid.MovePosition(m_Rigid.position + m_velocity * Time.deltaTime);

		if (m_rotation != Vector3.zero)
			m_Rigid.MoveRotation(m_Rigid.rotation * Quaternion.Euler(m_rotation));

		if (playerCamera != null)
			playerCamera.transform.Rotate(-m_cameraRotation);

	}

	private void Update()
	{
		if (Time.timeScale <= 0) return;

		IsGrounded = Physics.CheckSphere(groundCheckPosition.position, groundCheckRadius, groundLayers);

		if (Input.GetButtonDown("Jump") && IsGrounded)
		{
			m_Rigid.AddForce(transform.up * jumpForce, ForceMode.Impulse);
		}
	}
}