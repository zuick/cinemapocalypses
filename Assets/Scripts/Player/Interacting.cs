using UnityEngine;

namespace CinemaPocalypses
{
	public enum MouseButton { Left = 0, Right = 1, Middle = 2 };

	public class Interacting : MonoBehaviour
	{
		public float radius;
		public LayerMask interactableLayer;
		public Transform pointer;
		public MouseButton mouseButton;

		public Interactable InteractableAtPointer
		{
			get
			{
				return interactableAtPointer;
			}
			set
			{
				if (interactableAtPointer == value)
					return;

				if (interactableAtPointer != null)
					interactableAtPointer.ActorLeave(this);

				if (value != null)
					value.ActorEnter(this);

				interactableAtPointer = value;
			}
		}

		private Interactable interactableAtPointer;
		private GameObject interactableGameObjectAtPointer;

		private void Update()
		{
			if (Physics.Raycast(pointer.position, pointer.forward, out var hitInfo, radius, interactableLayer))
			{
				if (interactableGameObjectAtPointer != hitInfo.collider.gameObject)
				{
					interactableGameObjectAtPointer = hitInfo.collider.gameObject;
					InteractableAtPointer = interactableGameObjectAtPointer.GetComponent<Interactable>();
				}
			}
			else if (interactableGameObjectAtPointer != null)
			{
				interactableGameObjectAtPointer = null;
				InteractableAtPointer = null;
			}

			if (InteractableAtPointer != null && Input.GetMouseButtonDown((int)mouseButton))
			{
				InteractableAtPointer.Interact(this);
			}
		}
	}
}