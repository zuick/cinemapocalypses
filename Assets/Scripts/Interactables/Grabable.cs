using UnityEngine;

namespace CinemaPocalypses
{
	public class Grabable : Interactable
	{
		public Rigidbody body;
		public Sprite icon;
		
		public override void Interact(Interacting actor)
		{
			base.Interact(actor);

			var inventory = actor.gameObject.GetComponent<Inventory>();
			if (inventory != null)
				inventory.Add(this);
		}
	}
}