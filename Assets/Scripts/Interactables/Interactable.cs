using UnityEngine;
using UnityEngine.Events;
using CinemaPocalypses.Events;
using GameEventBus;

namespace CinemaPocalypses
{
	public class Interactable : MonoBehaviour
	{
		public bool intreactOnlyOnce = true;
		public UnityEvent OnInteract = new UnityEvent();
		public UnityEvent OnActorEnter = new UnityEvent();
		public UnityEvent OnActorLeave = new UnityEvent();

		public virtual void ActorEnter(Interacting actor)
		{
			EventBus.Instance.Publish(new ActorEnter
			{
				actor = actor,
				interactable = this
			});
			OnActorEnter.Invoke();
		}

		public virtual void ActorLeave(Interacting actor)
		{
			EventBus.Instance.Publish(new ActorLeave
			{
				actor = actor,
				interactable = this
			});
			OnActorLeave.Invoke();
		}

		public virtual void Interact(Interacting actor)
		{
			if (intreactOnlyOnce)
				enabled = false;

			OnInteract.Invoke();
		}
	}
}