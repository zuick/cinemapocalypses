using UnityEngine;
using CinemaPocalypses.Events;
using GameEventBus;

namespace CinemaPocalypses
{
	public class InteractingHint : MonoBehaviour
	{
		public GameObject hint;

		private void Awake()
		{
			EventBus.Instance.Subscribe<ActorEnter>(OnActorEnter);
			EventBus.Instance.Subscribe<ActorLeave>(OnActorLeave);
		}

		private void OnDestroy()
		{
			EventBus.Instance.Unsubscribe<ActorEnter>(OnActorEnter);
			EventBus.Instance.Unsubscribe<ActorLeave>(OnActorLeave);
		}

		private void OnActorEnter(ActorEnter e)
		{
			hint.SetActive(true);
		}

		private void OnActorLeave(ActorLeave e)
		{
			hint.SetActive(false);
		}
	}
}