﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
	public CanvasGroup canvas;
	public float fadeInTime;
	public float fadeOutTime;
	public Text caption;
	public bool fadeOutOnAwake = false;

	public static Fader Instance;

	public bool isFading { private set; get; }

	protected virtual void Awake()
	{
		if (Instance == null)
			Instance = this;

		if (fadeOutOnAwake)
		{
			canvas.alpha = 1f;
			FadeOut();
		}
	}

	public void SetCaption(string text)
	{
		if (caption == null)
			return;

		caption.text = text;
	}

	public void FadeIn(string caption, Action onComplete = null)
	{
		SetCaption(caption);
		if (!isFading)
		{
			isFading = true;
			StartCoroutine(Fading(1f, onComplete));
		}
	}

	public void FadeIn(Action onComplete = null)
	{
		FadeIn("", onComplete);
	}

	public void FadeOut(Action onComplete = null)
	{
		if (!isFading)
		{
			isFading = true;
			StartCoroutine(Fading(0f, onComplete));
		}
	}

	IEnumerator Fading(float endAlpha, Action onComplete = null)
	{
		var startAlpha = canvas.alpha;
		var fadeTime = startAlpha < endAlpha ? fadeInTime : fadeOutTime;
		while (canvas.alpha != endAlpha)
		{
			canvas.alpha += (endAlpha - startAlpha) * Time.deltaTime * 1f / fadeTime;
			yield return new WaitForEndOfFrame();
		}
		isFading = false;
		if (onComplete != null)
			onComplete.Invoke();
	}
}
