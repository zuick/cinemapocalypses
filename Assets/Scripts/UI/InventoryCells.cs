using UnityEngine;
using CinemaPocalypses.Events;
using GameEventBus;

namespace CinemaPocalypses
{
	public class InventoryCells : MonoBehaviour
	{
		public Transform pivot;
		public GameObject hint;
		public InventoryCell inventoryCellPrefab;

		private void Awake()
		{
			EventBus.Instance.Subscribe<InventoryChanged>(OnInventoryChanged);
		}

		private void OnDestroy()
		{
			EventBus.Instance.Unsubscribe<InventoryChanged>(OnInventoryChanged);
		}

		private void OnInventoryChanged(InventoryChanged e)
		{
			pivot.Clear();

			for (var i = 0; i < e.items.Count; i++)
			{
				var cell = Instantiate(inventoryCellPrefab, pivot, false);
				cell.Build(e.items[i], i == e.selectedItemIndex);
			}

			hint.SetActive(e.items[e.selectedItemIndex] != null);
		}
	}
}