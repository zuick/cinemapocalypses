using UnityEngine;
using UnityEngine.UI;
using CinemaPocalypses.Events;
using GameEventBus;

namespace CinemaPocalypses
{
	public class InventoryCell : MonoBehaviour
	{
		public Color normalColor;
		public Color selectedColor;

		public Image image;
		public Image border;

		public void Build(Grabable grabable, bool isSelected)
		{
			if (grabable != null)
				image.sprite = grabable.icon;

			image.enabled = grabable != null;
			border.color = isSelected ? selectedColor : normalColor;
		}
	}
}