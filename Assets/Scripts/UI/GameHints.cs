using UnityEngine;
using CinemaPocalypses.Events;
using CinemaPocalypses.L10n;
using GameEventBus;
using System;

namespace CinemaPocalypses
{
	public class GameHints : MonoBehaviour
	{
		public GameObject startGameWindow;
		public GameObject endGameWindow;
		
		private void Awake()
		{
			EventBus.Instance.Subscribe<GameStart>(OnGameStart);
			EventBus.Instance.Subscribe<GameEnd>(OnGameEnd);
		}

		private void OnDestroy()
		{
			EventBus.Instance.Unsubscribe<GameStart>(OnGameStart);
			EventBus.Instance.Unsubscribe<GameEnd>(OnGameEnd);
		}

		private void OnGameStart(GameStart e)
		{
			Pause();
		}

		private void Pause()
		{
			Cursor.visible = true;
			Time.timeScale = 0f;
			startGameWindow.SetActive(true);
			endGameWindow.SetActive(false);
		}

		public void Continue()
		{
			Cursor.visible = false;
			Time.timeScale = 1f;
			startGameWindow.SetActive(false);
			endGameWindow.SetActive(false);
		}
		
		public void SetEnglish()
		{
			L10nManager.CurrentLocale = "en";
		}

		public void SetRussian()
		{
			L10nManager.CurrentLocale = "ru";
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if(Time.timeScale < 1)
					Continue();
				else
					Pause();
			}
		}

		private void OnGameEnd(GameEnd e)
		{
			startGameWindow.SetActive(false);
			endGameWindow.SetActive(true);
		}
	}
}