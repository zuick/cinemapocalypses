﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class Teleport : MonoBehaviour
{
	public Transform arrivalTarget;
	public string[] filteredTags;
	public UnityEvent OnTeleport = new UnityEvent();

	protected virtual void OnTriggerEnter(Collider other)
	{
		DoTeleport(other.gameObject, arrivalTarget);
	}

	protected virtual void DoTeleport(GameObject obj, Transform target)
	{
		if (!this.filteredTags.Contains(obj.tag))
		{
			return;
		}

		if (Fader.Instance != null)
		{
			OnTeleport.Invoke();
			Fader.Instance.FadeIn(() =>
			{
				obj.transform.position = target.position;
				obj.transform.rotation = target.rotation;
				Fader.Instance.FadeOut();
			});
		}
		else
		{
			obj.transform.position = target.position;
		}
	}
}
