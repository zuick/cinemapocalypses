using UnityEngine;
using UnityEngine.Events;
using CinemaPocalypses.Events;
using GameEventBus;

namespace CinemaPocalypses
{
	public class QuestArea : MonoBehaviour
	{
		public int scene;
		public int id;

		public Transform questItemPivot;
		public UnityEvent OnCompleted = new UnityEvent();

		private void OnTriggerEnter(Collider other)
		{
			var questItem = other.gameObject.GetComponent<QuestItem>();
			if (questItem == null)
				return;
			else if (questItem.scene == scene && questItem.id == id)
			{
				EventBus.Instance.Publish(new QuestCompleted { scene = scene, id = id });
				questItem.OnCompleted.Invoke();
				OnCompleted.Invoke();
				if (questItemPivot != null)
				{
					other.transform.SetParent(questItemPivot, false);
					other.transform.localPosition = Vector3.zero;
					other.transform.rotation = questItemPivot.rotation;
				}
			}
		}
	}

}