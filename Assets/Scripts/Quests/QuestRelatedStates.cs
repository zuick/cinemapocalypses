using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using CinemaPocalypses.Events;
using GameEventBus;

namespace CinemaPocalypses
{
	public class QuestRelatedStates : MonoBehaviour
	{
		[Serializable]
		public class QuestToState
		{
			public int[] ids;
			public GameObject state;
		}

		public int scene;
		public Transform states;
		public QuestToState[] transitions;

		private List<int> completedQuests = new List<int>();
		private List<QuestToState> sortedTransitions = new List<QuestToState>();

		private void Awake()
		{
			EventBus.Instance.Subscribe<QuestCompleted>(OnQuestCompleted);

			SetStateTo(states.GetChild(0).gameObject);

			sortedTransitions = transitions
				.OrderByDescending(t => t.ids.Length)
				.ToList();
		}

		private void OnDestroy()
		{
			EventBus.Instance.Unsubscribe<QuestCompleted>(OnQuestCompleted);
		}

		private void OnQuestCompleted(QuestCompleted e)
		{
			if (scene != e.scene)
				return;

			completedQuests.Add(e.id);

			var transition = sortedTransitions.FirstOrDefault(
				t => t.ids.All(q => completedQuests.Contains(q))
			);

			if (transition != null)
				SetStateTo(transition.state);
		}

		private void SetStateTo(GameObject state)
		{
			foreach (Transform child in states)
				child.gameObject.SetActive(false);

			state.SetActive(true);
		}
	}
}