using UnityEngine;
using UnityEngine.Events;

namespace CinemaPocalypses
{
	public class QuestItem : MonoBehaviour
	{
		public int scene;
		public int id;

		public UnityEvent OnCompleted = new UnityEvent();
	}
}