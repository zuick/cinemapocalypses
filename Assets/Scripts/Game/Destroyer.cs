﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public string[] filteredTags;

    void OnTriggerEnter(Collider other)
    {
        if(!this.filteredTags.Contains(other.gameObject.tag)) {
            return;
        }

        Destroy(other.gameObject);
    }
}
