using GameEventBus.Events;
using UnityEngine;
using System.Collections.Generic;

namespace CinemaPocalypses.Events
{
	public class ActorEnter : EventBase
	{
		public Interacting actor;
		public Interactable interactable;
	}

	public class ActorLeave : EventBase
	{
		public Interacting actor;
		public Interactable interactable;
	}

	public class Interacted : EventBase
	{
		public Interacting actor;
		public Interactable interactable;
	}

	public class InventoryChanged : EventBase
	{
		public List<Grabable> items;
		public int selectedItemIndex;
	}

	public class QuestCompleted : EventBase
	{
		public int scene;
		public int id;
	}

	public class GameStart : EventBase
	{

	}

	public class GameEnd : EventBase
	{

	}

	public class LocaleChanged : EventBase
	{
		public string locale;
	}
}