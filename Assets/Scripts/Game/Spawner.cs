﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject spawnGameObject;
    public float delay;

    void Start()
    {
        StartCoroutine(SpawnGameObject());
    }

    private void Repeat()
    {
        StartCoroutine(SpawnGameObject());
    }

    private IEnumerator SpawnGameObject()
    {
        yield return new WaitForSeconds(delay);

        GameObject car = Instantiate(spawnGameObject, transform.position, transform.rotation);
        car.SetActive(true);

        Repeat();
    }
}
