using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;

public static class StringExtensions
{
	private static Regex newLineReplacer = new Regex(@"\\n", RegexOptions.IgnoreCase);
	private static Regex newLineEmptySpace = new Regex("\n ", RegexOptions.IgnoreCase);

	public static string ReplaceNewLine(this string str)
	{
		return newLineEmptySpace.Replace(newLineReplacer.Replace(str, "\n"), "\n");
	}
}