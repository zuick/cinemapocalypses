using UnityEngine;
using GameEventBus;
using System.Collections.Generic;
using System.Linq;
using System;
using CinemaPocalypses.Events;
using UnityEngine.SceneManagement;

namespace CinemaPocalypses
{
	public class SceneController : MonoBehaviour
	{
		[Serializable]
		public class QuestCondition
		{
			public int scene;
			public List<int> ids = new List<int>();
		}

		public QuestCondition[] endGameConditions;
		private List<QuestCondition> completed = new List<QuestCondition>();

		private void Awake()
		{
			Cursor.visible = false;
			EventBus.Instance.Subscribe<QuestCompleted>(OnQuestCompleted);
		}

		private void Start()
		{
			EventBus.Instance.Publish(new GameStart());
		}

		private void OnDestroy()
		{
			EventBus.Instance.Unsubscribe<QuestCompleted>(OnQuestCompleted);
		}

		private void OnQuestCompleted(QuestCompleted e)
		{
			var cq = completed.FirstOrDefault(c => c.scene == e.scene);
			if(cq == null)
			{
				cq = new QuestCondition{ scene = e.scene };
				completed.Add(cq);
			}

			if(!cq.ids.Contains(e.id))
				cq.ids.Add(e.id);

			if(endGameConditions.All(c => {
					var quests = completed.FirstOrDefault(q => q.scene == c.scene);
					return quests != null && c.ids.Except(quests.ids).Count() == 0; 
				}))
				EventBus.Instance.Publish(new GameEnd());
		}
	}
}