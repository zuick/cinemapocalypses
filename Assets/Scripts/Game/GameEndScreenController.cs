using UnityEngine;
using UnityEngine.SceneManagement;

namespace CinemaPocalypses
{
	public class GameEndScreenController : MonoBehaviour
	{
		private void FixedUpdate()
		{
			if(Input.GetKeyDown(KeyCode.Escape))
				Application.Quit();

			if(Input.GetKeyDown(KeyCode.R))
				SceneManager.LoadScene(gameObject.scene.name);
		}
	}
}