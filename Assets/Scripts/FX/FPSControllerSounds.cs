using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class FPSControllerSounds : MonoBehaviour
{
	public FPSController fpsController;
	public float randomizePitching = 0.05f;
	public float randomizeVolume = 0.05f;
	public float panEffect = 0.1f;
	public float stepsByMeter = 2f;

	public AudioClip[] clips;

	private AudioSource source;
	private bool leftStep;
	private float volume = 1f;

	private void Awake()
	{
		source = GetComponent<AudioSource>();
	}

	private void Start()
	{
		if (clips.Length > 0 && fpsController != null)
		{
			volume = source.volume;
			StartCoroutine(StepsSoundsCoroutine());
			StartCoroutine(GroundedSoundsCoroutine());
		}
	}

	IEnumerator StepsSoundsCoroutine()
	{
		while (enabled && stepsByMeter > 0f)
		{
			yield return new WaitUntil(
				() => fpsController.State != FPSControllerStates.Staying &&
					fpsController.State != FPSControllerStates.InJumping
			);

			yield return new WaitForSeconds((1f / stepsByMeter) / fpsController.Speed);

			if (fpsController.State == FPSControllerStates.Staying) continue;

			DoStepSound();

			leftStep = !leftStep;

			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator GroundedSoundsCoroutine()
	{
		while (enabled)
		{
			if (!fpsController.IsGrounded)
			{
				yield return new WaitUntil(() => fpsController.IsGrounded);
				DoStepSound();
			}

			yield return new WaitForEndOfFrame();
		}
	}

	private void DoStepSound()
	{
		source.clip = clips[UnityEngine.Random.Range(0, clips.Length)];
		source.panStereo = leftStep ? -panEffect : panEffect;
		source.pitch = 1f + UnityEngine.Random.Range(-randomizePitching, randomizePitching);
		source.volume = volume + UnityEngine.Random.Range(-randomizeVolume, randomizeVolume);
		source.Play();
	}
}