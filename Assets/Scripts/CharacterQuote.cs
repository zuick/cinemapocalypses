﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using CinemaPocalypses.L10n;

public class CharacterQuote : MonoBehaviour
{
	public bool randomize;
	public float randomDelay = 0f;
	public Text text;
	public float interval;
	public float duration;

	public string[] quotes;

	IEnumerator Start()
	{
		var index = 0;
		if(quotes.Length == 0)
		{
			text.text = "";
			yield break;
		}

		yield return new WaitForSeconds(UnityEngine.Random.Range(0, randomDelay));

		while (true)
		{	
			text.text = L10nManager.Get(quotes[randomize ? UnityEngine.Random.Range(0, quotes.Length) : index], true);
			yield return new WaitForSeconds(duration);
			text.text = "";
			yield return new WaitForSeconds(interval);
			index = index >= quotes.Length - 1 ? 0 : index + 1;
		}
	}
}
