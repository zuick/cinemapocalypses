using UnityEngine;
using UnityEngine.UI;

namespace CinemaPocalypses
{
	public class ChangeText : MonoBehaviour
	{
		public Text target;
		public string text;
		public bool add = true;

		private void OnEnable()
		{
			target.text = add ? $"{target.text }\n{text}" : text;
		}
	}
}