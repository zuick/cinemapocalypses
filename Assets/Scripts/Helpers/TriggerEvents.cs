using UnityEngine;
using UnityEngine.Events;
using System.Linq;

namespace CinemaPocalypses
{
	public class TriggerEvents : MonoBehaviour
	{
		public UnityEvent OnEnter = new UnityEvent();
		public UnityEvent OnExit = new UnityEvent();
		public string[] filteredTags;

		private void OnTriggerEnter(Collider other)
		{
			if (!filteredTags.Contains(other.gameObject.tag))
				return;

			OnEnter.Invoke();
		}

		private void OnTriggerExit(Collider other)
		{
			if (!filteredTags.Contains(other.gameObject.tag))
				return;

			OnEnter.Invoke();
		}
	}
}