using UnityEngine;

namespace CinemaPocalypses
{
	public class LookAtMainCamera : MonoBehaviour
	{
		private void Update()
		{
			transform.LookAt(Camera.main.transform, Vector3.up);
		}
	}
}