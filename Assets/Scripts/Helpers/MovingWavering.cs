using UnityEngine;

public class MovingWavering : MonoBehaviour
{
	public enum MovingAxis
	{
		x, y, z
	}

	public Transform target;
	public MovingAxis axis = MovingAxis.x;
	public float amplitude = 1f;
	public float period = 1f;
	public bool restoreOnDisable = true;
	public bool startAtRandomPosition = false;

	private Vector3 oldPosition;
	private float counter;

	private void OnEnable()
	{
		if (target == null)
			target = transform;

		oldPosition = target.localPosition;
		if(startAtRandomPosition)
			counter = Random.Range(0, period);
	}

	private void OnDisable()
	{
		if (restoreOnDisable)
			target.localPosition = oldPosition;
	}

	private void Update()
	{
		counter += Time.deltaTime / period;
		var delta = amplitude * Mathf.Sin(counter);

		var newPosition = oldPosition;
		if (axis == MovingAxis.x)
			newPosition.x += delta;
		if (axis == MovingAxis.y)
			newPosition.y += delta;
		if (axis == MovingAxis.z)
			newPosition.z += delta;

		target.localPosition = newPosition;
	}
}