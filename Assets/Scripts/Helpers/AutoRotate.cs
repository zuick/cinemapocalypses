using UnityEngine;

namespace CinemaPocalypses
{
	public class AutoRotate : MonoBehaviour
	{
		public float Speed = 0.1f;
		public Vector3 Angle = Vector3.up;

		void Update()
		{
			transform.Rotate(Angle, Speed);
		}
	}
}